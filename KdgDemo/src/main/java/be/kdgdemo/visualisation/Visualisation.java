package be.kdgdemo.visualisation;

/**
 * @author Jo Somers
 */
public interface Visualisation {

    public void showError(String message);

    public void cancelForActivity();

}
