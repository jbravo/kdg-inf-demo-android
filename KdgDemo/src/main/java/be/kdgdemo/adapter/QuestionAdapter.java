package be.kdgdemo.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.common.collect.Ordering;

import java.util.List;

import be.kdgdemo.R;
import be.kdgdemo.model.Question;
import butterknife.ButterKnife;
import butterknife.InjectView;

import static android.view.LayoutInflater.from;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @author Jo Somers
 */
public class QuestionAdapter extends BaseAdapter {

    private final static Ordering<Question> QUESTION_ORDERING = new Ordering<Question>() {
        @Override
        public int compare(Question left, Question right) {
            return right.getModified().compareTo(left.getModified());
        }
    };

    private final Context context;

    private List<Question> questions;

    public QuestionAdapter(Context context) {
        this.context = context;
        this.questions = newArrayList();
    }

    public void setQuestions(List<Question> questions) {
        this.questions = QUESTION_ORDERING.sortedCopy(questions);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        this.questions.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return questions.size();
    }

    @Override
    public Question getItem(int position) {
        return questions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Question question = getItem(position);

        ViewHolder viewHolder;
        if (convertView != null) {
            viewHolder = (ViewHolder) convertView.getTag();
        } else {
            convertView = from(context).inflate(R.layout.question_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        viewHolder.modifiedTextView.setText(question.getModified().toString("dd-MM-yyyy hh:mm"));
        viewHolder.questionTextView.setText(question.getQuestion());
        viewHolder.numberOfAnswersTextView.setText("" + question.getAnswers().size());

        return convertView;
    }

    static class ViewHolder {
        @InjectView(R.id.modified)
        TextView modifiedTextView;

        @InjectView(R.id.question)
        TextView questionTextView;

        @InjectView(R.id.numberOfAnswers)
        TextView numberOfAnswersTextView;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

}
