package be.kdgdemo.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.lang.reflect.Type;

import static be.kdgdemo.json.Iso8601DateTimeTypeAdapter.ISO_8601_DATE_TIME_TYPE_ADAPTER;
import static be.kdgdemo.json.Iso8601LocalDateTypeAdapter.ISO_8601_LOCAL_DATE_TYPE_ADAPTER;

/**
 * @author Jo Somers
 */
public class GsonHelper<T> {

    private final Type type;

    private final Gson gson;

    public GsonHelper(final Type type) {
        this.gson = new GsonBuilder()
                .registerTypeAdapter(DateTime.class, ISO_8601_DATE_TIME_TYPE_ADAPTER)
                .registerTypeAdapter(LocalDate.class, ISO_8601_LOCAL_DATE_TYPE_ADAPTER)
                .create();

        this.type = type;
    }

    public T fromJson(final String json) {
        return gson.fromJson(json, type);
    }

    public String toJson(final T object) {
        return gson.toJson(object);
    }

}